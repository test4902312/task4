public class Simple {
    public double a = 10.0;
    private double b = 20.0;

    public Simple(){

    }

    public Simple(double a, double b){
        this.a = a;
        this.b = b;
    }

    public void squareA(){
        this.a *= this.a;
    }

    private void squareB(){
        this.b *=this.b;
    }

    public double getA(){
        return a;
    }

    private void setA(double a){
        this.a = a;
    }

    public void setB(double b){
        this.b = b;
    }

    public double getB(){
        return b;
    }

    public String toString(){
        return String.format("(a:%s, b:%s)", a, b);
    }

}
